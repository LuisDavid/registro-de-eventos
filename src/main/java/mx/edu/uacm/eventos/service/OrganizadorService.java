package mx.edu.uacm.eventos.service;

import org.springframework.stereotype.Service;

import mx.edu.uacm.eventos.domain.Organizador;

@Service
public interface OrganizadorService {
	public boolean guardarOrganizador(Organizador o);
	public Organizador validarOrganizador(String email,String contrasena);
}
