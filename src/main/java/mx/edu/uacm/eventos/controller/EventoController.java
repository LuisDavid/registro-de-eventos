package mx.edu.uacm.eventos.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import mx.edu.uacm.eventos.domain.Evento;
import mx.edu.uacm.eventos.domain.Organizador;
import mx.edu.uacm.eventos.service.EventoService;

@Controller
@SessionAttributes("evento")
/**
 * 
 * @author Luis David
 * Duración de implementación 6 horas incluido la creación de la vista de crearEvento.
 *
 */
public class EventoController {
	private static final Logger LOG = LogManager.getLogger(EventoController.class);
	@Autowired
	private EventoService eventoService;
	@Autowired
	private HttpSession httpSession;
	
	@GetMapping("/home")
	public String getHome(Model model) {
		Organizador organizador=(Organizador) httpSession.getAttribute("organizador");
		if(organizador!=null ) {
			List<Evento> eventos=eventoService.obtenerEventosPorOrganizador(organizador.getId());
			model.addAttribute("eventos",eventos);
			return "home";
		}
		return "login";
	}
	
	@GetMapping("/registrarEvento")
	public String mandarVistaRegistro() {
		Organizador organizador=(Organizador) httpSession.getAttribute("organizador");
		if(organizador != null) 
			return "registrarEvento";
		
		return "login";
	}
	
	@GetMapping("/actualizarEvento")
	public String mandarVistaActualizar(Model model,Long idEvento) {
		Organizador organizador=(Organizador) httpSession.getAttribute("organizador");
		if(organizador != null) {
			Evento e= eventoService.obtenerEvento(idEvento);
			model.addAttribute(e);
			return "editarEvento";
		}
		return "login";
	}
	@GetMapping("/consultarEvento")
	public String consultarEvento(Model model,Long idEvento) {
		Evento e= eventoService.obtenerEvento(idEvento);
		model.addAttribute(e);
		return "vistaEvento";
	}
	@PostMapping("/registrarEvento")
	public String registrarEvento(Evento e,@RequestParam("programaEvento") MultipartFile imagen) throws IOException {
		LOG.debug("nombre: "+e.getNombre());
		LOG.debug("fecha: "+e.getFechaCierreRegistro());
		Organizador organizador=(Organizador) httpSession.getAttribute("organizador");
		if(organizador != null) {
			eventoService.guardarEvento(e);
			String ruta="";
			if(!imagen.isEmpty()) {
				byte[] bytes=imagen.getBytes();
				String nombreArchivo=imagen.getOriginalFilename();
				ruta=".//src//main//resources//files//"+e.getId()+"."+nombreArchivo.split("\\.")[1];
				Path path= Paths.get(ruta);
				Files.write(path,bytes);
				LOG.debug("archivo subido: "+imagen.getOriginalFilename()+" dasd: "+nombreArchivo.split("\\.").length);
			}
			e.setUrlPrograma(ruta);
			e.setOrganizador(organizador);
			eventoService.guardarEvento(e);
			return "registrarEvento";
		}
		return "login";
	}
	
}
