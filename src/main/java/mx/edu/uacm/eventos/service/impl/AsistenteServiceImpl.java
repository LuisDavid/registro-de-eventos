package mx.edu.uacm.eventos.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.uacm.eventos.domain.Asistente;
import mx.edu.uacm.eventos.repository.AsistenteRepository;
import mx.edu.uacm.eventos.service.AsistenteService;

@Service
public class AsistenteServiceImpl implements AsistenteService{
	@Autowired
	private AsistenteRepository bd;
	
	@Override
	public boolean guardaAsistente(Asistente a) {
		if(bd.save(a)!=null)
			return true;
		return false;
	}

}
