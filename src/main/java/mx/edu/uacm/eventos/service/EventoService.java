package mx.edu.uacm.eventos.service;

import java.util.List;

import org.springframework.stereotype.Service;

import mx.edu.uacm.eventos.domain.Evento;

@Service
public interface EventoService {
	public boolean guardarEvento(Evento e);
	public Evento obtenerEvento(Long id);
	public int obtenerNumParticipantes(Long id);
	public List<Evento> obtenerEventosPorOrganizador(Long idOrganizador);
}
