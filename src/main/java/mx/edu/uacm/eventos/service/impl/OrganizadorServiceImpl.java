package mx.edu.uacm.eventos.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.uacm.eventos.domain.Organizador;
import mx.edu.uacm.eventos.repository.OrganizadorRepository;
import mx.edu.uacm.eventos.service.OrganizadorService;

@Service
public class OrganizadorServiceImpl implements OrganizadorService{
	@Autowired
	private OrganizadorRepository bd;
	@Override
	public boolean guardarOrganizador(Organizador o) {
		if(bd.save(o)!=null)
			return true;
		return false;
	}
	@Override
	public Organizador validarOrganizador(String email,String contrasena) {
		Organizador or= bd.findByEmailAndPassword(email, contrasena);
		return or;
	}

}
