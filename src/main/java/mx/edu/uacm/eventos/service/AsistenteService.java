package mx.edu.uacm.eventos.service;

import org.springframework.stereotype.Service;

import mx.edu.uacm.eventos.domain.Asistente;

@Service
public interface AsistenteService {
	public boolean guardaAsistente(Asistente a);
}
