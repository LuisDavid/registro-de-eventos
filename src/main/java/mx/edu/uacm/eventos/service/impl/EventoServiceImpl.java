package mx.edu.uacm.eventos.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.edu.uacm.eventos.domain.Evento;
import mx.edu.uacm.eventos.repository.EventoRepository;
import mx.edu.uacm.eventos.service.EventoService;

@Service
public class EventoServiceImpl implements EventoService{
	@Autowired
	private EventoRepository bd;
	@Override
	public boolean guardarEvento(Evento e) {
		if(bd.save(e)!=null)
			return true;
		return false;
	}
	@Override
	public Evento obtenerEvento(Long id) {
		return bd.findById(id).orElse(null);
	}
	@Override
	public int obtenerNumParticipantes(Long id) {
		
		return bd.getNumParticipantes(id);
	}
	@Override
	public List<Evento> obtenerEventosPorOrganizador(Long idOrganizador) {
		List<Evento> eventos=bd.obtenerEventosPorIdOrganizador(idOrganizador);
		if(eventos!=null) {
			for(Evento e: eventos) {
				e.setNumAsistentesRegistrados(obtenerNumParticipantes(e.getId()));
			}
		}
		return eventos;
	}
	
}
