package mx.edu.uacm.eventos.controller;

import org.springframework.web.bind.annotation.GetMapping;

public class AsistenteController {
	
	@GetMapping("/registrarAsistencia")
	public String mandarVistaRegistrarAsistencia() {
		return "registrarAsistencia";
	}	

}
