package mx.edu.uacm.eventos.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import mx.edu.uacm.eventos.Application;
import mx.edu.uacm.eventos.repository.OrganizadorRepository;



@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class OrganizadorServiceTest {
	@Autowired
	private OrganizadorRepository org;
	@Test
	public void guardarOrganizadorTest() {
		Organizador o= new Organizador();
		o.setNombre("Luis");
		o.setApellidos("Perez");
		o.setCorreo("luis@gmail.com");
		o.setPassword("dasdafadDa");
		org.save(o);
	}
}
