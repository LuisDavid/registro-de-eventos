package mx.edu.uacm.eventos.controller;

import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import mx.edu.uacm.eventos.domain.Organizador;
import mx.edu.uacm.eventos.service.OrganizadorService;
/**
 * 
 * @author Luis David
 * Este contralador fui terminado en 6 horas debido aque hubo problemas en la consulta para la
 * validación del usaurio a iniciar sesión.
 *
 */
@Controller
public class AccesoController {
	private static final Logger LOG = LogManager.getLogger(AccesoController.class);
	
	@Autowired
	HttpSession httpSession;
	@Autowired
	OrganizadorService organizadorService;
	private String mensaje;
	@ModelAttribute("mensaje")
    public String mensaje() {
        return mensaje;
    }
	@GetMapping("/login")
	public String mandarVistaLogin() {
		LOG.debug("Login vista");
		return "login";
	}
	@PostMapping("/login")
	public String iniciarSesion(String correo, String password) {
		Organizador org=organizadorService.validarOrganizador(correo, password);
		//LOG.debug("correo: "+correo+"  pass:; " +password);
		if(org!=null) {
			httpSession.setAttribute("login",true);
			httpSession.setAttribute("organizador", org);
			return "home";
            //httpSession.removeAttribute("userLoggedIn");
        }
        mensaje="Usuario y contraseña incorrectos";
        
		
		return "redirect:/login";
	}
	@PostMapping("/crearCuenta")
	public String crearCuenta(Organizador org) {
		
		organizadorService.guardarOrganizador(org);
		
		return "index";
	}
	@GetMapping("/salir")
	public String cerrar() {
		mensaje="";
		try {
			httpSession.invalidate();
		}
		catch(IllegalStateException e) {
			return "error";
		}
		return "index";
	}
}
