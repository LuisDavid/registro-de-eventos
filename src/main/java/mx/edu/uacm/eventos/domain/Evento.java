package mx.edu.uacm.eventos.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
@Entity
public class Evento {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nombre;
	private String motivo;
	private int numParticipantes;
	private String lugar;
	private Date fechaInicioEvento;
	private Date fechaFinEvento;
	private Date fechaInicioRegistro;
	private Date fechaCierreRegistro;
	private String urlPrograma;
	private String notas;
	
	@Transient//Este atributo no se concidera para la BD.
	private int numAsistentesRegistrados;
	
	@OneToOne
	private Organizador organizador;
	@OneToMany(cascade=CascadeType.ALL,targetEntity=Asistente.class)
	private List<Asistente> asistentes;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public int getNumParticipantes() {
		return numParticipantes;
	}
	public void setNumParticipantes(int numParticipantes) {
		this.numParticipantes = numParticipantes;
	}
	public Date getFechaInicioEvento() {
		return fechaInicioEvento;
	}
	public void setFechaInicioEvento(String fechaInicioEvento) {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		try {
			this.fechaInicioEvento = formato.parse(fechaInicioEvento);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public Date getFechaCierreRegistro() {
		return fechaCierreRegistro;
	}
	public void setFechaCierreRegistro(String fechaCierreRegistro) {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		try {
			this.fechaCierreRegistro = formato.parse(fechaCierreRegistro);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public String getUrlPrograma() {
		return urlPrograma;
	}
	public void setUrlPrograma(String urlPrograma) {
		this.urlPrograma = urlPrograma;
	}
	public String getNotas() {
		return notas;
	}
	public void setNotas(String notas) {
		this.notas = notas;
	}
	public List<Asistente> getAsistentes() {
		return asistentes;
	}
	public void setAsistentes(List<Asistente> asistentes) {
		this.asistentes = asistentes;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Organizador getOrganizador() {
		return organizador;
	}
	public void setOrganizador(Organizador organizador) {
		this.organizador = organizador;
	}
	public String getLugar() {
		return lugar;
	}
	public void setLugar(String lugar) {
		this.lugar = lugar;
	}
	public Date getFechaFinEvento() {
		return fechaFinEvento;
	}
	public void setFechaFinEvento(String fechaFinEvento) {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		try {
			this.fechaFinEvento = formato.parse(fechaFinEvento);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public Date getFechaInicioRegistro() {
		return fechaInicioRegistro;
	}
	public void setFechaInicioRegistro(String fechaInicioRegistro) {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		try {
			this.fechaInicioRegistro = formato.parse(fechaInicioRegistro);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public int getNumAsistentesRegistrados() {
		return numAsistentesRegistrados;
	}
	public void setNumAsistentesRegistrados(int numAsistentesRegistrados) {
		this.numAsistentesRegistrados = numAsistentesRegistrados;
	}

	
	
	
}
