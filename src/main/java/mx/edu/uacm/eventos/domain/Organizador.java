package mx.edu.uacm.eventos.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
//@PrimaryKeyJoinColumn(name="persona_id")
public class Organizador extends Persona{
	private String password;
	@OneToMany
	private List<Evento> eventos;
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Evento> getEventos() {
		return eventos;
	}

	public void setEventos(List<Evento> eventos) {
		this.eventos = eventos;
	}
	
}
