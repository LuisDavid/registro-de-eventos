package mx.edu.uacm.eventos;

import org.h2.server.web.WebServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

import mx.edu.uacm.eventos.domain.Organizador;
import mx.edu.uacm.eventos.repository.OrganizadorRepository;

/** La clase con un solo método estatico para inicializar toda la aplicación web.
 *
 * 
 * @author Luis David
 *
 */

@SpringBootApplication
public class Application {
	@Autowired
	private OrganizadorRepository org;
  /**
   * 
   * @param args Argumentos
   */
  public static void main(final String[] args) {
    SpringApplication.run(Application.class, args);
  }
  /**
   * 
   * @return ServletRegistrationBean 
   */
  @Bean
  public ServletRegistrationBean<WebServlet> h2servletRegistration() {
	  Organizador o= new Organizador();
		o.setNombre("Luis");
		o.setApellidos("Perez");
		o.setCorreo("luisdaviduacm@gmail.com");
		o.setPassword("123456");
	  org.save(o);
      final ServletRegistrationBean<WebServlet> registration 
        = new ServletRegistrationBean<WebServlet>(new WebServlet());
      registration.addUrlMappings("/console/*");
      return registration;
  }
}
