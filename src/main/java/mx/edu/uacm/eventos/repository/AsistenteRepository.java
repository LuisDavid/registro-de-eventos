package mx.edu.uacm.eventos.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import mx.edu.uacm.eventos.domain.Asistente;

@Repository
public interface AsistenteRepository extends CrudRepository<Asistente,Long> {


}
