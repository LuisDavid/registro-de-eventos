package mx.edu.uacm.eventos.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mx.edu.uacm.eventos.domain.Evento;

@Repository
public interface EventoRepository extends CrudRepository<Evento,Long> {
	@Query(value="select count(asistentes_id) from evento_asistentes where evento_id=:id",nativeQuery=true)
	int getNumParticipantes(@Param("id")Long id);
	@Query(value="select * from evento where organizador_id=:idOrganizador",nativeQuery=true)
	List<Evento> obtenerEventosPorIdOrganizador(@Param("idOrganizador")Long idOrganizador);
}
