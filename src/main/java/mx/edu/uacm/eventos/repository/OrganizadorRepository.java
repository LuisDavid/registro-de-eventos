package mx.edu.uacm.eventos.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import mx.edu.uacm.eventos.domain.Organizador;


@Repository
@Transactional
public interface OrganizadorRepository extends CrudRepository<Organizador,Long> {
	@Query(value="select * from persona where correo=:correo and password=:contrasena",nativeQuery=true)
	Organizador findByEmailAndPassword(@Param("correo")String email,@Param("contrasena")String contrasena);
}
